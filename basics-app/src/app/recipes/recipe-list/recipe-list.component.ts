import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Recipe} from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

	recipes: Recipe[] = [
		new Recipe('A Test Recipe', 'This is a simply a test', 'https://www.bbcgoodfood.com/sites/default/files/recipe-collections/collection-image/2013/05/frying-pan-pizza-easy-recipe-collection.jpg'),

		new Recipe('Kebab', 'Delicious beef kebab', 'https://res.cloudinary.com/hellofresh/image/upload/f_auto,fl_lossy,h_436,q_auto/v1/hellofresh_s3/image/enchiladas-aux-legumes-1a1102aa.jpg'),

		new Recipe('Delicious Macarony', 'Macarony with tomatoes and vegetables', 'https://images.media-allrecipes.com/images/56589.png')

	];

	@Output() recipeWasSelected = new EventEmitter<Recipe>();

	onRecipeSelected(recipe: Recipe) {
		this.recipeWasSelected.emit(recipe);
	}

  constructor() { }

  ngOnInit() {
  }

}
