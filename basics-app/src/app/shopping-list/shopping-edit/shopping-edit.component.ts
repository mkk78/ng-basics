import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Ingredient } from '../../shared/ingredient.model';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {

  @Output() ingredientCreated = new EventEmitter<Ingredient>();

  constructor() { }

  onAddIngredient(nameInput: HTMLInputElement, amountInput: HTMLInputElement) {
  	this.ingredientCreated.emit(new Ingredient(nameInput.value, +amountInput.value));
  }

  ngOnInit() {
  }

}
